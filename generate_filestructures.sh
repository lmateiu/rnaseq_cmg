
mkdir samples
ddir=/home/shared_data_medgen_frax/RNAseq_Elisa/run_data/171201_NB501809_0217_AHVLGTAFXX

for i in $(ls $ddir|grep gz$|awk -F "_S[[:digit:]]*_" '{print $1}'|sort|uniq);do \
mkdir -p samples/$i/fastq \
bash -c "ln -s $ddir/$i"_"*".fastq.gz" samples/$i/fastq/" \
done


for samplein in $(ls samples );do \
qsub -N rna_${samplein} -o $(pwd)/logs/${samplein}.o.txt -e $(pwd)/logs/${samplein}.e.txt -F ${samplein} ./run_pipe_by_sample_quantseq_star.sh; \
done
