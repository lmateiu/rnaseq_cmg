#!/bin/bash
#PBS -l nodes=1:mips:ppn=16,mem=50G,walltime=24:00:00
#PBS -m abe
#PBS -M ligia.mateiu@uantwerpen.be
#PBS -V
cd $PBS_O_WORKDIR

ref_version=mm10
REFERENCE=/opt/NGS/References/mm10/fasta/mm10.fa
GTFFILE=/opt/NGS/References/mm10/refgene/mm10.ensGene.gtf
GENOMEINDEX=/opt/NGS/References/mm10/star/mm10.star
ADAPTERFILE=/opt/software/miniconda_rnaseq/opt/bbmap-38.86-0/resources
BINPATH=/opt/software/miniconda_rnaseq/bin

np=16

LIBSETCOUNT="-s reverse"
RGLB=QuantSeq 
RGPL=NextSeq
RGPU=mm10
RGSM=${sample}.mm

samplefolder=$1
sample=${samplefolder}
wd=$(pwd)
mkdir -p $wd/samples/${sample}/logs 

echo "pipeline from https://www.lexogen.com/quantseq-data-analysis/ for QuantSeq 3' libraries" >>$wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
echo "analysis run by Ligia">>$wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
echo $(date)>$wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"



# # # # # # # 

##QC
echo "Running QC on the demultiplexed fastqs with FastQC...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"

mkdir -p $wd/samples/${sample}/qc
str=$(ls $wd/samples/${sample}/fastq/*.gz|tr '\n' ' ')
zcat $str |grep ^@|awk -F":" '{print $NF}'|sort|uniq -c|sort -k1,1nr >$wd/samples/${sample}/logs/${sample}.barcode_summary.txt
zcat $str| $BINPATH/fastqc stdin -t $np -o $wd/samples/${sample}/qc --extract
mv $wd/samples/${sample}/qc/stdin_fastqc $wd/samples/${sample}/qc/${sample}"_"fastqc
mv $wd/samples/${sample}/qc/stdin_fastqc.html $wd/samples/${sample}/qc/${sample}"_"fastqc.html
rm $wd/samples/${sample}/qc/stdin_fastqc.zip
sed -i "s/stdin/${sample}/g" $wd/samples/${sample}/qc/${sample}"_"fastqc.html
sed -i "s/stdin/${sample}/g" $wd/samples/${sample}/qc/${sample}"_"fastqc/summary.txt
sed -i "s/stdin/${sample}/g" $wd/samples/${sample}/qc/${sample}"_"fastqc/fastqc_report.html
sed -i "s/stdin/${sample}/g" $wd/samples/${sample}/qc/${sample}"_"fastqc/fastqc_data.txt
sed -i "s/stdin/${sample}/g" $wd/samples/${sample}/qc/${sample}"_"fastqc/fastqc.fo


##trimming and cleaning
echo "Running trimming and cleaning fastqs with bbduk...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"

(zcat $wd/samples/${sample}/fastq/*.fastq.gz |$BINPATH/bbduk.sh in=stdin.fq out=stdout.fq ref=$ADAPTERFILE/polyA.fa.gz,$ADAPTERFILE/truseq.fa.gz k=13 ktrim=r useshortkmers=t mink=5 qtrim=r trimq=10 minlength=20 int=f ziplevel=9 stats=logs/${sample}.contaminants.txt |gzip  >$wd/samples/${sample}/${sample}_trimmed.fastq.gz ) 2> $wd/samples/${sample}/logs/${sample}.bbduk.log
 
##QC_trimmed
echo "Running QC on the trimmed fastqs with FastQC... ">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
$BINPATH/fastqc $wd/samples/${sample}/${sample}_trimmed.fastq.gz -t $np  --extract -o $wd/samples/${sample}/qc/

##alignment 
echo "Running alignment with star...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
$BINPATH/STAR --runThreadN $np --genomeDir $GENOMEINDEX --readFilesIn $wd/samples/${sample}/${sample}_trimmed.fastq.gz \
--outFilterType BySJout --outFilterMultimapNmax 20 --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 \
--readFilesCommand zcat --outFilterMismatchNmax 999 -outFilterMismatchNoverLmax 0.1 --alignIntronMin 20 \
--alignIntronMax 1000000 --alignMatesGapMax 1000000 --outSAMattributes NH HI NM MD \
--outSAMtype BAM SortedByCoordinate --outFileNamePrefix $wd/samples/${sample}/${sample}_

if ! grep -q "ALL DONE!" "$wd/samples/${sample}/${sample}_Log.progress.out"; then
 echo "Alignment not properly finished!" >> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
fi


rm -rf $wd/samples/${sample}/*_STARtmp
mv $wd/samples/${sample}/*.out $wd/samples/${sample}/logs

##
echo "Selecting uniquepy mapped reads and indexing bam ...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
$BINPATH/samtools view -bq 255  $wd/samples/${sample}/${sample}_Aligned.sortedByCoord.out.bam >samples/${sample}/${sample}.${ref_version}.bam

mv $wd/samples/${sample}/*.tab $wd/samples/${sample}/logs
$BINPATH/samtools index $wd/samples/${sample}/${sample}.${ref_version}.bam

##
# echo "Running alignment diagnostic ...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
# # # # (read_distribution.py -r $RSEQFILES/Homo_sapiens.GRCh38.87.bed12 -i $wd/msamples/${sample}/${sample}-hisat-mm38.bam)   2> $logfolder/${sample}.aligndiag.txt   1>> $logfolder/${sample}.aligndiag.txt  
# ($BINPATH/bam_stat.py -i $wd/samples/${sample}/${sample}.${ref_version}.bam ) 2>> $wd/samples/${sample}/logs/${sample}.aligndiag.txt  1>> $wd/samples/${sample}/logs/${sample}.aligndiag.txt  


##
echo "Running transcripts quantification ...">> $wd/samples/${sample}/logs/"log_rnaseq_pipeline.txt"
 ($BINPATH/featureCounts -T $np  -t exon -g gene_id -s 1 -a $GTFFILE -o  $wd/samples/${sample}/${sample}.featcounts.tsv  $wd/samples/${sample}/${sample}.${ref_version}.bam ) 2> $wd/samples/${sample}/logs/${sample}.featcounts.txt  

 


###predict gender in human data
# genecount="$wd/samples/${sample}/${sample}.featcounts.tsv" 
# male=$(cat $genecount |grep -e ENSG00000129824 -e ENSG00000198692 -e ENSG00000067048 -e ENSG00000012817 |awk '{sum+=$NF} END {print sprintf("%.0f",sum/4)}')
# female=$(cat $genecount |grep ENSG00000229807|awk '{print $NF}')
# 
# percentmale=$(awk -v v1="$male" -v v2="$female" 'BEGIN{print v1*100/(v1+v2)}')
# percentfemale=$(awk -v v1="$male" -v v2="$female" 'BEGIN {print v2*100/(v1+v2)}')
